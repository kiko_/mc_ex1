import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBB2c12IbOPVf6eQclAzh7fxj0vQlB17UE",
  authDomain: "mc-ex1.firebaseapp.com",
  projectId: "mc-ex1",
  storageBucket: "mc-ex1.appspot.com",
  messagingSenderId: "395701458435",
  appId: "1:395701458435:web:a1e332ed7cf7ae5cfca036"
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);