import React, { useEffect, useState } from 'react';
import { StyleSheet, TextInput ,Button, Text, View, SafeAreaView, SectionList, StatusBar, Image } from "react-native";

import {db} from "./firebase-config";
import {
  collection,
  getDocs,
  setDoc,
  addDoc,
  updateDoc,
  deleteDoc,
  doc,
  onSnapshot
} from "firebase/firestore";

const App = () => {
  const [route, setRoute] = useState("home");
  const [users, setUsers] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    const colRef = collection(db, "users");
    onSnapshot(colRef, (querySnapshot)=> {
      const users = [];
      querySnapshot.forEach((doc) => {
        const {name, email, phone} = doc.data();
        users.push({
          id: doc.id,
          name,
          email,
          phone
        })
      });

      setUsers(users);
    })
  })


  const routing = (value) => {
    setRoute(value);
  }
  const accessDetails = (name, email, phone) => {
    setName(name);
    setEmail(email);
    setPhone(phone)
  }
  const createUser = (name, email, phone) => {
    addDoc(collection(db, "users"), { name: name, email: email, phone: phone });
    setRoute("home");
  }
  
  const selectUser = () => {
    
  }

  const deleteUser = (email) => {
    deleteDoc((doc(db, "users", email)));
  }
  const updateUser = (email, name, phone) => {
    //
  }

  return (
    <SafeAreaView style={styles.container}>
      {
        route === "createUser"
        ? 
          <CreateUser routing={routing} createUser={createUser}/>
        : (
          route === "userDetails"
          ?
          <UserDetails name={name} email={email} phone={phone} deleteUser={deleteUser} updateUser={updateUser} routing={routing}/>
          :
          <UserList accessDetails={accessDetails} users={users} routing={routing}/>
        )
      }
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  item: {
    backgroundColor: "#ffffff",
    paddingBottom: 10,
    // marginVertical: 0,
    borderBottomWidth:1,
    borderColor: "#A2A2A2",
    marginHorizontal:-1
  },
  header: {
    fontSize: 32,
    backgroundColor: "#ffffff",
    paddingTop: 10,
    paddingBottom: 30,
    marginHorizontal:0,
    paddingLeft: 20
  },
  username: {
    fontSize: 24,
    paddingLeft: 90
  },
  mail: {
    fontSize: 17,
    paddingLeft: 95
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

const UserList = ({users, routing, accessDetails}) => {
  return (
    <SafeAreaView style={styles.item}>
      <Text style={styles.header}>Users List</Text>
      <Button
        title="CREATE USER"
        onPress={() => routing("createUser")}
      />
      {
        users.map((user) => {
          return <DisplayItem accessDetails={accessDetails} routing={routing} email={user.email} name={user.name} phone={user.phone}/>
        })
      }
  </SafeAreaView>
  );
}

const DisplayItem = ({phone, email, name, routing, accessDetails}) => {
  return(
    <View style={styles.item}>
       {/* <Image
        style={styles.tinyLogo}
        source={pictureURL}
      /> */}
      <Text style={styles.username} onPress={() => {
        accessDetails(phone, name, email);
        routing("userDetails");
      }
      }>{name}</Text>
    </View>
  );
}

const UserDetails = ({name1, email1, phone1, routing, deleteUser, updateUser}) => {
  const [name, setName] = useState(name1);
  const [email, setEmail] = useState(email1);
  const [phone, setPhone] = useState(phone1);

  return (
    <View style={styles.container}>
      <Button title="Go back" onPress={()=> routing("home")}></Button>
      <Text style={styles.header}>User Details</Text>
      

      <TextInput
        style={styles.input}
        onChangeText={text => setName(text)}
        value={name}
        placeholder={name}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        onChangeText={text => setEmail(text)}
        value={email}
        placeholder={email}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        onChangeText={text => setPhone(text)}
        value={phone}
        placeholder={phone}
        keyboardType="numeric"
      />

      <Button title="UPDATE USER" onPress={()=> {
          updateUser(email, name, phone)  
        }
      }></Button>
      
      <Button title="DELETE USER" onPress={()=> {
          deleteUser(email);
        }
      }></Button>
    
    </View>
  )  
}

const CreateUser = ({createUser, routing}) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  return (
    <View style={styles.container}>
      <Button title="Go back" onPress={()=> routing("home")}></Button>
      <Text style={styles.header}>Create a New User</Text>
      
      <TextInput
        style={styles.input}
        onChangeText={text => setName(text)}
        value={name}
        placeholder="Name"
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        onChangeText={text => setEmail(text)}
        value={email}
        placeholder="Email"
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        onChangeText={text => setPhone(text)}
        value={phone}
        placeholder="Image URL"
        keyboardType="numeric"
      />

      <Button title="SAVE USER" onPress={()=> {
        if(name !== "" && email !== "" && phone !== "")
          createUser(name, email, phone)
        }
        }></Button>
    </View>
  ) 
}


export default App;